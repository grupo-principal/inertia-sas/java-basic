public class Casting {
    public static void main(String[] args) {

        // En un año ubicar 30 perritos
        // Cuántos perritos ubiqué al mes

        double montlyDogs = 30.0/12.0;
        System.out.println(montlyDogs);

        //ESTIMACIÓN
        int estimatedMontlyDogs = (int) montlyDogs;
        System.out.println(estimatedMontlyDogs);

        // EXACTITUD
        int a = 30;
        int b = 12;
        System.out.println((double) a/b);

        double c = a/b;
        System.out.println(c);

        char n = '1';
        int nI = n;
        System.out.println(nI);

        short nS = (short) n;
        System.out.println(nS);
    }
}
