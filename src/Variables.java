public class Variables {
    public static void main(String[] args) {
        // Declarando una variable
        int speed;

        // Asignamos un valor a una varaible
        speed = 10;
        System.out.println(speed);

        // Declaramos y asignamos una variable
        int salary = 1000;
        String employeeName = "Luis Camilo Jimenez";
        System.out.println(employeeName);
    }
}
