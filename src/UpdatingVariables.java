public class UpdatingVariables {
    public static void main(String[] args) {
        int salary = 1000;

        // Bono por $200 pesos
        salary += 200;
        System.out.println(salary);

        // Descuento por pensión por $50 pesos
        salary-= 50;
        System.out.println(salary);

        // Actualizar cadenas de texto
        String empoyeeName = "Luis Camilo";
        empoyeeName = empoyeeName + " Jimenez";
        System.out.println(empoyeeName);
    }
}
